# Egocentric git view

A visulisation designed to show how someone, or a group of people have affected a git repository.

Code is at https://gitlab.com/ninjakiwi/egocentric-git-view (Private until after project submission date)


## How to use

A demo is available at https://ninjakiwi.gitlab.io/egocentric-git-view/

To view one of your own projects download this project to your computer and run `./git_to_json.py /path/to/your/project/ > ./resources/git_data.json` (It will use the default branch). Then run a file server in this directory. (You can use `python3 -m http.server` from the python std). Alternatively, you can fork this project on gitlab, and use gitlab pages to host a site. Now open the page on a browser and you should see your project loaded into the page.

This visualisation may struggle with very large projects that have a long history because it attempts to load the statistics for every commit on that branch into memory.
