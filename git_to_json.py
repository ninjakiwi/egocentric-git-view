#!/bin/python3
#
# A python script that takes a git project path,
# and returns a json array containing a summary of each commit.
#
# Requires git to be installed and in the PATH.
#
# Usage: git_to_json.py <target_project_path>
# 
# You will likely want to pipe the output into a file:
#     ./git_to_json.py /path/to/project/ > ./git_data.json

import subprocess
import sys
import json
import re
import copy

if len(sys.argv) != 2:
    print("Usage: git_to_json.py <target_project_path>\n", sys.stderr)
    exit(1)

project_path = sys.argv[1]

# The actual git call to obtain the information about the repository.
git_call = subprocess.Popen(['git', '--no-pager', '-C', project_path,
                             'log', '--numstat', '--date=short',
                             '--format=|| %h || %s || %ad || %an',
                             '--no-merges'],
                            stdout=subprocess.PIPE, text=True)
git_output, err = git_call.communicate()


final_object = []
commit_data = {'short_hash': None, 'comment': '', 'date': '', 'author': '', 'files': []}
lines = git_output.split('\n')

for line in lines:
    if line is None:
        break
    elif line == '':
        continue

    if line.startswith('|| '):
        # Commit previous commit data to array (if just on the first commit)
        if commit_data['short_hash'] is not None:
            final_object.append(copy.deepcopy(commit_data))
        info_line = line.strip('| ').split(' || ')
        commit_data['files'] = []
        commit_data['short_hash'] = info_line[0]
        commit_data['comment'] = info_line[1]
        commit_data['date'] = info_line[2]
        commit_data['author'] = info_line[3]
    else:
        file_line = re.split('[ \t]+', line)
        file = {'file_name': file_line[2], 'added_lines': file_line[0], 'removed_lines': file_line[1]}
        commit_data['files'].append(file)

# Add the last commit
final_object.append(copy.deepcopy(commit_data))

# Convert python object to json and print.
print(json.dumps(final_object))
