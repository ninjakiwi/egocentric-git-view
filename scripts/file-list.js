// Add the file selector module to the global object
(function(root, factory) {

    root.FileSelector = factory();

    window.addEventListener('load', () => FileSelector.initialise());

})(this, function() {

    'use strict';

    let FileSelector,
        hover_target = null,
        html_container = null;

    /**
     * Initialises the module, gets the target element on the page.
     */
    function initialise() {

        let containers = document.getElementsByClassName('filelist');

        if (containers.length != 1) {
            console.error('Cannot find element/s');
            return;
        }

        html_container = document.createElement('div');
        html_container.classList.add('filelist-inner');
        
        containers[0].appendChild(html_container);

    }

    /**
     * Creates the list of files in the html
     */
    function setAllFiles(file_list) {

        file_list.sort();

        for (let file of file_list) {
            
            let container = document.createElement('div');
            container.classList.add('select-box');
            let text = document.createElement('p');
            text.textContent = file;

            container.addEventListener('click', () => {
                container.classList.toggle('selected');
            });
            container.addEventListener('dblclick', () => {
                let all_files_in_container = html_container.children;
                for (let html_file of all_files_in_container) {
                    html_file.classList.remove('selected');
                }
                container.classList.add('selected');
            });
            container.addEventListener('mouseover', () => {
                hover_target = file;
                window.dispatchEvent(new Event('egocentric-filehover'));
            });
            container.addEventListener('mouseleave', () => {
                hover_target = null;
                window.dispatchEvent(new Event('egocentric-filehoverend'));
            });

            container.appendChild(text);
            html_container.appendChild(container);

        }

    }

    /**
     * Given a list of commits, highlight the files that are modified in any of the commits.
     */
    function setHighlighted(commits) {

        if (commits === null) {
            html_container.classList.remove('highlight-mode');
            return;
        } else {
            html_container.classList.add('highlight-mode');
        }

        let file_list = [];
        for (let commit of commits) {
            for (let file of commit['files']) {
                if (!file_list.includes(file['file_name'])) {
                    file_list.push(file['file_name']);
                }
            }
        }

        let files = html_container.children;

        for (let html_file of files) {

            if (file_list.includes(html_file.textContent)) {
                html_file.classList.add('highlight');
            } else {
                html_file.classList.remove('highlight');
            }

        }

    }

    /**
     * Get the list of files that the user has selected
     */
    function getSelected() {

        let files = html_container.children;
        let selected = [];

        for (let html_file of files) {

            if (html_file.classList.contains('selected')) {
                selected.push(html_file.textContent);
            }

        }

        if (selected.length === 0) return null;

        return selected;

    }

    function getTargets() {

        if (hover_target) {
            return [hover_target];
        } else {
            
            return getSelected();
        }

    }


    // ------------------------- //
    // ---------- API ---------- //
    // ------------------------- //


    FileSelector = {
        initialise,
        setAllFiles,
        getTargets,
        setHighlighted,
    }

    return FileSelector;

});