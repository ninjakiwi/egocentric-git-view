// Variable to store all commits straight from loading the resource in.
// Used to reset the timeline.
let all_commits = [];

window.addEventListener('egocentric-timelineLoaded', () => {

    console.log('loaded');

    // Get the git data from the server and load it into the modules.
    fetch('resources/git_data.json')
        .then(data => data.json())
        .then(data => {
            data.reverse();
            all_commits = data;

            TargetSelector.setAllAuthors(getAllAuthors(data));

            FileSelector.setAllFiles(getAllFiles(data));

            Timeline1D.loadData(data);

            Timeline.reset();
        });

});

/**
 * Hover event listeners
 * 
 * The getTargets function in any module should return null (for no selection),
 * an empty list (for an empty selection) or a list of commits.
 * 
 * If an item has a hover event, the getTargets function should return a list of the hovered commits.
 * 
 * The setHighlighted function in any module should accept a list of commits and highlight those commits in the module.
 * If an empty list is provided, nothing should be highlighted, whereas if null is provided, there is no specific selection (highlight everything)
 */

window.addEventListener('egocentric-newTargetsSelected', () => {
    Timeline.setTargets(TargetSelector.getTargets() || []);
    Timeline.reload();
    let selected_files = getCommitsFromFiles(FileSelector.getTargets());
    let selected_authors = getCommitsFromAuthors(TargetSelector.getTargets());
    Timeline.setHighlighted(selected_files);
    FileSelector.setHighlighted(selected_authors);
    Timeline1D.setHighlighted(commitListAND(selected_files, selected_authors));
});
window.addEventListener('egocentric-filehover', (event) => {
    let selected_commits = getCommitsFromFiles(FileSelector.getTargets());
    Timeline.setHighlighted(selected_commits);
    TargetSelector.setHighlighted(selected_commits);
    Timeline1D.setHighlighted(selected_commits);
});
window.addEventListener('egocentric-filehoverend', (event) => {
    let selected_commits = getCommitsFromFiles(FileSelector.getTargets());
    Timeline.setHighlighted(selected_commits);
    TargetSelector.setHighlighted(selected_commits);
    Timeline1D.setHighlighted(commitListAND(selected_commits, getCommitsFromAuthors(TargetSelector.getTargets())));
});
window.addEventListener('egocentric-timelinehover', (event) => {
    let selected_commits = Timeline.getTargets();
    TargetSelector.setHighlighted(selected_commits);
    FileSelector.setHighlighted(selected_commits);
    Timeline1D.setHighlighted(selected_commits);
});
window.addEventListener('egocentric-timelinehoverend', (event) => {
    let selected_commits = getCommitsFromFiles(FileSelector.getTargets());
    let selected_authors = getCommitsFromAuthors(TargetSelector.getTargets());
    TargetSelector.setHighlighted(selected_commits);
    FileSelector.setHighlighted(selected_authors);
    Timeline1D.setHighlighted(commitListAND(selected_commits, selected_authors));
});

/**
 * From a list of commits, return a list of all the authors involved.
 */
function getAllAuthors(commits) {

    let all_authors = [];

    for (let commit of commits) {
        if (!all_authors.includes(commit['author'])) {
            all_authors.push(commit['author']);
        }
    }

    return all_authors;

}

/**
 * From a list of commits, get a list of all files involved.
 */
function getAllFiles(commits) {

    let all_files = [];

    for (let commit of commits) {
        for (let file of commit['files']) {
            if (!all_files.includes(file['file_name'])) {
                all_files.push(file['file_name']);
            }
        }
    }

    return all_files;

}

/** 
 * From a list of files, get the commits where all the files were modified
 * (AND gate, each commit must modify EVERY file in the list)
 */
function getCommitsFromFiles(target_files) {

    if (!target_files) return null;

    let select_commits = [];

    for (let commit of all_commits) {
        let all_files_match_commit = true;

        let commit_files = commit['files'].map(a => a['file_name']);

        for (let file of target_files) {
            if (!commit_files.includes(file)) {
                all_files_match_commit = false;
            }
        }

        if (all_files_match_commit) {
            select_commits.push(commit);
        }
    }

    return select_commits;

}

/**
 * Returns list of commits that are in both lists
 */
function commitListAND(commits1, commits2) {
    if (!commits1) return commits2;
    if (!commits2) return commits1;
    let result = commits1.filter(a => commits2.includes(a));
    if (result == []) result = null;
    return result
}

/**
 * From a list of authors, get all the commits modified by any of the provided authors.
 * (OR (inclusive) gate, a commit authored by any one author is returned)
 */
function getCommitsFromAuthors(target_authors) {

    if (target_authors === null) return null;

    let select_commits = [];

    for (let commit of all_commits) {

        if (target_authors.includes(commit['author'])) {
            select_commits.push(commit);
        }

    }

    return select_commits;

}

/**
 * Given a commit, how many lines were removed and added.
 */
function getTotalChanges(commit) {

    let total_additions = 0,
        total_removals = 0;
    
    for (let file of commit['files']) {
        total_additions += Number(file['added_lines']);
        total_removals += Number(file['removed_lines']);
    }

    return [total_additions, total_removals];

}


/**
 * Group functions
 * 
 * A "Group" is a time period that commits are grouped into.
 * The smallest time period provided, 'commit', is not a linear time scale, as commits can be submitted at any interval.
 */

// All possible groupings for the timelines
let GROUPS = ['year', 'month', 'day', 'commit'];

/**
 * Return the next group type with a smaller time period
 */
function nextGroup(current_group) {
    
    let index = GROUPS.indexOf(current_group);

    if (index + 1 < GROUPS.length) {
        return GROUPS[index + 1];
    } else {
        return current_group;
    }

}

/**
 * Given a commit, and group type, return the group id.
 * 
 * A group id is something that will be the same for all commits in that group
 * and is defined in this function. For example, if grouping by year, the group id is
 * the year itself. (eg. 2017)
 */
function getGroupID(commit, group_by) {

    switch (group_by) {
        case 'commit':
            return commit['short_hash'];
        case 'year':
            // return just the year
            return commit['date'].split('-')[0];
        case 'day':
            // return the whole date
            return commit['date'];
        case 'month':
        default:
            // return the year and the month
            return commit['date'].split('-').filter((item, index) => index != 2).join('-');
    }

}

/**
 * Returns the next group id (in terms of time) if possible.
 * If not applicable for the group (like 'commit'), null is returned.
 */
function getNextGroupID(group_id, group_by) {

    switch (group_by) {
        case 'commit':
            return null;
        case 'year':
            // return just the year + 1
            return Number(group_id.split('-')[0]) + 1 + '';
        case 'day':
            // return the next day
            let date = new Date(group_id);
            date.setDate(date.getDate() + 1);
            return date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + (date.getDate()).toString().padStart(2, '0');
        case 'month':
        default:
            // return the year and the month + 1
            let date2 = new Date(group_id);
            date2.setMonth(date2.getMonth() + 1);
            return date2.getFullYear() + '-' + (date2.getMonth() + 1).toString().padStart(2, '0');
    }

}

/**
 * Returns all groups between two other groups (in terms of time)
 */
function getAllGroupsBetween(group_id1, group_id2, group_by) {

    if (group_by === 'commit' || group_id1 === group_id2) return [];

    let current_group_id = getNextGroupID(group_id1, group_by);
    let in_between = [];

    while (current_group_id !== null && current_group_id != group_id2) {
        in_between.push(current_group_id);

        current_group_id = getNextGroupID(current_group_id, group_by);
    }

    return in_between;

}
