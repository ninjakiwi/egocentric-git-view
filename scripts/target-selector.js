// Add the target selector module to the global object
(function(root, factory) {

    root.TargetSelector = factory();

    window.addEventListener('load', () => TargetSelector.initialise());

})(this, function() {

    'use strict';

    let TargetSelector,
        html_selector = null,
        hover_targets = null,
        current_targets = null;

    /**
     * Initialises the module, gets the target element on the page.
     */
    function initialise() {

        let selectors = document.getElementsByClassName('target-selector');

        if (selectors.length != 1) {
            console.error('Cannot find element/s for "target-selector" module (or too many)');
            return;
        }

        html_selector = selectors[0];

    }

    /**
     * Given a list of authors, add them onto the html page.
     */
    function setAllAuthors(author_list) {

        for (let author of author_list) {
            
            let container = document.createElement('div');
            container.classList.add('select-box');
            let text = document.createElement('p');
            text.textContent = author;

            container.addEventListener('click', () => {
                container.classList.toggle('selected');

                current_targets = [];
    
                for (let target of html_selector.children) {
                    if (target.classList.contains('selected')) {
                        current_targets.push(target.firstChild.textContent);
                    }
                }

                if (current_targets.length === 0) current_targets = null;
            });
            container.addEventListener('dblclick', () => {
                for (let target of html_selector.children) {
                    if (target.classList.contains('selected')) {
                        target.classList.remove('selected');
                    }
                }
                container.classList.add('selected');
            });
            container.addEventListener('mouseover', () => {
                hover_targets = [text.textContent];
                window.dispatchEvent(new Event('egocentric-newTargetsSelected'));
            });
            container.addEventListener('mouseleave', () => {
                hover_targets = [];
    
                for (let target of html_selector.children) {
                    if (target.classList.contains('selected')) {
                        hover_targets.push(target.firstChild.textContent);
                    }
                }

                if (hover_targets.length === 0) {
                    hover_targets = null;
                }

                window.dispatchEvent(new Event('egocentric-newTargetsSelected'));
            });

            container.appendChild(text);
            html_selector.appendChild(container);

        }

    }

    function getTargets() {

        if (hover_targets !== null) {
            return hover_targets;
        } else {
            return current_targets;
        }

    }

    function setHighlighted(commits) {

        if (commits === null) {
            html_selector.classList.remove('highlight-mode');
            return;
        } else {
            html_selector.classList.add('highlight-mode');
        }
        
        let user_list = [];
        for (let commit of commits) {
            if (!user_list.includes(commit['author'])) {
                user_list.push(commit['author']);
            }
        }

        let users = html_selector.children;

        for (let html_user of users) {

            if (user_list.includes(html_user.textContent)) {
                html_user.classList.add('highlight');
            } else {
                html_user.classList.remove('highlight');
            }

        }

    }


    // ------------------------- //
    // ---------- API ---------- //
    // ------------------------- //


    TargetSelector = {
        initialise,
        setAllAuthors,
        getTargets,
        setHighlighted,
    }

    return TargetSelector;

});