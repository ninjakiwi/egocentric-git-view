// Add the timeline module to the global object
(function(root, factory) {

    root.Timeline = factory();

    window.addEventListener('load', () => Timeline.initialise());

})(this, function() {

    'use strict';

    let Timeline;

    let html_container = null,
        html_inner_container = null,
        html_timeline = null,
        html_title = null,
        html_y_upperlimit = null,
        html_y_lowerlimit = null,
        html_x_upperlimit = null,
        html_x_lowerlimit = null,
        html_x_title = null,

        // Group formatting functions (turning a group id into text used on the display)
        format_year = new Intl.DateTimeFormat('en-NZ', { year: 'numeric' }).format,
        format_month = new Intl.DateTimeFormat('en-NZ', { month: 'short', year: 'numeric' }).format,
        format_day = new Intl.DateTimeFormat('en-NZ', { dateStyle: 'short' }).format,

        data = [],
        target_users = [],
        hover_targets = [],
        last_highlighted = null,

        // The default (most zoomed out) view grouping
        default_group_by = 'day',
        group_by = '';

    /**
     * Initialises the module, gets the target element on the page.
     */
    function initialise() {

        group_by = default_group_by;

        let timelines = document.getElementsByClassName('timeline');
        let titles = document.getElementsByClassName('timeline-title');
        let y_upperlimit = document.getElementsByClassName('timeline-y-upper-limit');
        let y_lowerlimit = document.getElementsByClassName('timeline-y-lower-limit');
        let x_upperlimit = document.getElementsByClassName('timeline-x-upper-limit');
        let x_lowerlimit = document.getElementsByClassName('timeline-x-lower-limit');
        let x_title = document.getElementsByClassName('timeline-x-title');

        if (timelines.length != 1 && titles.length != 1 && y_upperlimit.length != 1 && y_lowerlimit.length != 1 &&
            x_upperlimit.length != 1 && x_lowerlimit.length != 1 && x_title.length != 1) {
            console.log('Cannot find element');
            return;
        }

        html_container = timelines[0];
        html_title = titles[0];
        html_y_upperlimit = y_upperlimit[0];
        html_y_lowerlimit = y_lowerlimit[0];
        html_x_upperlimit = x_upperlimit[0];
        html_x_lowerlimit = x_lowerlimit[0];
        html_x_title = x_title[0];

        // create inner container
        html_inner_container = document.createElement('div');
        html_inner_container.classList.add('timeline-inner');

        // create inner timeline container
        html_timeline = document.createElement('div');
        html_timeline.classList.add('timeline-graph');

        // Add to page
        html_inner_container.appendChild(html_timeline);
        html_container.appendChild(html_inner_container);

        window.dispatchEvent(new Event('egocentric-timelineLoaded'));

    }

    /**
     * Sets the users to target on the timeline.
     * 
     * Module is made to support more than one target at a time due to people changing git accounts
     */
    function setTargets(targets) {

        target_users = targets;

    }

    /** 
     * Resets the timeline to it's default grouping and reloads all commits.
     */
    function reset() {

        group_by = default_group_by;
        loadData(all_commits);

    }

    /**
     * Reloads the timeline using the commits in the data variable.
     */
    function reload() {

        loadData(data);

    }

    /**
     * Returns the data currently being used by the timeline.
     */
    function getData() {
        return data;
    }

    /**
     * Loads some data into the timeline
     * 
     * The data must already be in the order that it's to be displayed in
     */
    function loadData(new_data) {

        data = new_data;

        let view_all_button = document.getElementsByClassName('view_all')[0];
        if (data.length !== all_commits.length) {
            view_all_button.classList.remove('disabled');
        } else {
            view_all_button.classList.add('disabled');
        }

        let current_group_id = getGroupID(data[0], group_by);
        let first_group = current_group_id;
        let current_group = [];
        let full_height = 0;
        let bar_index = 0;

        for (let commit of data) {

            let new_group_id = getGroupID(commit, group_by);

            if (new_group_id != current_group_id) {

                let bar_height = addBar(current_group, bar_index, formatGroupID(current_group_id, true));
                bar_index += 1;
                full_height = Math.max(bar_height, full_height);
                current_group = [];

                // Add empty bars for groups with no commits (between current_group_id and new_group_id)
                let gap = getAllGroupsBetween(current_group_id, new_group_id, group_by).length;
                for (let i = 0; i < gap; i++) {
                    addBar([], bar_index);
                    bar_index += 1;
                }

                current_group_id = new_group_id;
            }

            current_group.push(commit);
            
        }

        let bar_height = addBar(current_group, bar_index, formatGroupID(current_group_id, true));
        bar_index += 1;
        full_height = Math.max(bar_height, full_height);

        html_x_lowerlimit.textContent = formatGroupID(first_group);
        html_x_upperlimit.textContent = formatGroupID(current_group_id);
        html_y_upperlimit.textContent = full_height / 2;
        html_y_lowerlimit.textContent = -full_height / 2;
        html_title.textContent = getTitle();

        html_x_title.textContent = 'Time';
        if (formatGroupID(first_group) === '') {
            html_x_title.textContent = 'Time - ' + format_day(new Date(getGroupID(new_data[0], 'day')));
        }

        html_timeline.style.height = (100 / (full_height / 100)) + '%';

        // Remove any extra bars that are not used (happens when new data is input with less bars)
        let current_bars = html_timeline.children.length;
        for (let index = current_bars; bar_index < index; index--) {
            html_timeline.removeChild(html_timeline.lastChild);
        }

        // Rehighlight/unhighlight any commits that were highlighted/not highlighted before the redraw.
        setHighlighted(last_highlighted);
        setView(0);

        window.dispatchEvent(new Event('egocentric-timelineReloaded'));

    }

    /**
     * Sets the timeline view horizontal scroll
     * 
     * how_far_scrolled - decimal 0-1 for how far through the conten.
     */
    function scrollView(how_far_scrolled) {
        let number_of_bars = html_timeline.children.length;
        let on_screen = howManyBarsFitOnScreen();
        let offset = Math.floor((number_of_bars - on_screen) * (how_far_scrolled));
        setView(offset);
    }

    /**
     * Sets the timeline view horizontal scroll
     * 
     * offset - number of bars from the first bar
     */
    function setView(offset) {

        let bars = html_timeline.children;
        let on_screen = howManyBarsFitOnScreen();

        let set_upper = false;

        for (let i = 0; i < bars.length; i++) {

            let bar = bars[i];
            
            if (i >= offset && i < (offset + on_screen)) {
                // Show
                bar.classList.remove('hidden');

                // Set the scale text
                if (i === Math.floor(offset)) {
                    if (bar.commits.length > 0)
                        html_x_lowerlimit.textContent = formatGroupID(getGroupID(bar.commits[0], group_by));
                } else if (i - 1 == (offset + on_screen)) {
                    set_upper = true;
                    if (bar.commits.length > 0)
                        html_x_upperlimit.textContent = formatGroupID(getGroupID(bar.commits[0], group_by));
                }
            } else {
                // Hide
                bar.classList.add('hidden');
            }

        }

        // If bars do not fill the whole graph, use the group for the scale's upper limit.
        if (!set_upper) {
            html_x_upperlimit.textContent = formatGroupID(getGroupID(bars[bars.length - 1].commits[0], group_by));
        }

    }

    /**
     * Return how many bars fit on the graph.
     */
    function howManyBarsFitOnScreen() {
        return Math.floor((html_inner_container.clientWidth - 10) / 50);
    }

    /**
     * Return the % of data shown on the graph.
     */
    function viewScale() {

        let number_of_bars_on_screen = howManyBarsFitOnScreen();
        let number_of_bars = html_timeline.children.length;

        return number_of_bars_on_screen / number_of_bars;
        
    }

    /**
     * Provided a group, get a appropriate title
     */
    function getTitle() {

        switch (group_by) {
            case 'commit':
                return 'Lines Changed Per Commit';
            case 'year':
                // return just the year
                return 'Lines Changed Per Year';
            case 'day':
                // return the whole date
                return 'Lines Changed Per Day';
            case 'month':
            default:
                // return the year and the month
                return 'Lines Changed Per Month';
        }

    }

    /**
     * Format a group id to a human readable format.
     */
    function formatGroupID(group_id, not_scale=false) {

        switch (group_by) {
            case 'commit':
                if (not_scale) {
                    let commit = getCommitByID(group_id);
                    return format_day(new Date(commit['date'])) + ' - ' + commit['comment'];
                }
                return '';
            case 'year':
                // return just the year
                return format_year(new Date(group_id));
            case 'day':
                // return the whole date
                return format_day(new Date(group_id));
            case 'month':
            default:
                // return the year and the month
                return format_month(new Date(group_id));
        }

    }

    // Get a entire commit from a commit hash.
    function getCommitByID(commit_id) {

        for (let commit of data) {
            if (commit['short_hash'] == commit_id) {
                return commit;
            }
        }

        return null;

    }

    /**
     * Adds a bar to the timeline
     */
    function addBar(commits, index, tooltip) {

        let target_additions = 0,
            target_removals = 0,
            other_additions = 0,
            other_removals = 0;

        for (let commit of commits) {
            let [additions, removals] = getTotalChanges(commit);
            if (target_users.includes(commit['author'])) {
                target_additions += additions;
                target_removals += removals;
            } else {
                other_additions += additions;
                other_removals += removals;
            }
        }

        let html_bar;
        let padding_top;
        let padding_bottom;
        let actual_bar_container;
        let html_addition_other;
        let html_addition_target;
        let html_removal_target;
        let html_removal_other;

        let reusing_elements = false;

        // If a bar already exists, use it rather than replacing it so we get an animation.
        if (html_timeline.children.length > index) {
            reusing_elements = true;
            html_bar = html_timeline.children[index];
    
            padding_top = html_bar.children[0];
            padding_bottom = html_bar.children[2];
            actual_bar_container = html_bar.children[1];
    
            html_addition_other = actual_bar_container.children[0];
            html_addition_target = actual_bar_container.children[1];
            html_removal_target = actual_bar_container.children[2];
            html_removal_other = actual_bar_container.children[3];
        } else {
            html_bar = document.createElement('div');
            html_bar.classList.add('hidden');
    
            padding_top = document.createElement('div');
            padding_bottom = document.createElement('div');
            actual_bar_container = document.createElement('div');
    
            html_addition_other = document.createElement('div');
            html_addition_target = document.createElement('div');
            html_removal_target = document.createElement('div');
            html_removal_other = document.createElement('div');
        }

        html_bar.classList.add('timeline-bar');
        actual_bar_container.classList.add('timeline-bar-realsize');

        html_addition_other.classList.add('timeline-bar-total-added');
        html_addition_other.dataset.count = other_additions;
        html_addition_other.style.flex = other_additions;

        html_addition_target.classList.add('timeline-bar-target-added');
        html_addition_target.dataset.count = target_additions;
        html_addition_target.style.flex = target_additions;

        html_removal_target.classList.add('timeline-bar-target-removed');
        html_removal_target.dataset.count = target_removals;
        html_removal_target.style.flex = target_removals;

        html_removal_other.classList.add('timeline-bar-total-removed');
        html_removal_other.dataset.count = other_removals;
        html_removal_other.style.flex = other_removals;

        let top_height = other_additions + target_additions;
        let bottom_height = target_removals + other_removals;
        let bar_height = top_height + bottom_height;
        let max_height = Math.max(bottom_height, top_height) * 2;

        html_bar.style.height = max_height + '%';
        actual_bar_container.style.flex = bar_height;

        // Use padding to make the bar float so the green and red bars meet at the middle (at 0) on the graph.
        padding_top.style.flex = Math.max(bottom_height - top_height, 0);
        padding_bottom.style.flex = Math.max(top_height - bottom_height, 0);

        if (!reusing_elements) {

            actual_bar_container.addEventListener('click', clickBarEvent);
            actual_bar_container.addEventListener('mouseover', setHoverTargetsEvent);
            actual_bar_container.addEventListener('mouseleave', clearHoverTargetsEvent);

            actual_bar_container.appendChild(html_addition_other);
            actual_bar_container.appendChild(html_addition_target);
            actual_bar_container.appendChild(html_removal_target);
            actual_bar_container.appendChild(html_removal_other);
    
            html_bar.appendChild(padding_top);
            html_bar.appendChild(actual_bar_container);
            html_bar.appendChild(padding_bottom);

            html_timeline.appendChild(html_bar);

        }
        
        html_bar.commits = commits;
        actual_bar_container.title = tooltip;

        return max_height;
    }

    /**
     * Zoom in on group on click
     */
    function clickBarEvent(event) {

        let new_group_by = nextGroup(group_by);

        if (group_by === new_group_by) {
            // Already at closest zoom level/next group doesn't exist
            return;
        } else {
            group_by = new_group_by;
        }

        let targets = event.currentTarget.parentNode.commits;
        loadData(targets);

        reload();

    }

    function setHoverTargetsEvent(event) {

        hover_targets = event.currentTarget.parentNode.commits;
        window.dispatchEvent(new Event('egocentric-timelinehover'));

    }

    function clearHoverTargetsEvent() {

        hover_targets = [];
        window.dispatchEvent(new Event('egocentric-timelinehoverend'));

    }

    /**
     * Highlight all bars that contain one of the provided commits
     */
    function setHighlighted(commits) {

        last_highlighted = commits;

        if (commits === null) {
            html_timeline.classList.remove('highlight-mode');
            return
        } else {
            html_timeline.classList.add('highlight-mode');
        }

        let commit_ids = commits.map((a) => a['short_hash']);
        let bars = html_timeline.children;

        for (let bar of bars) {
            let bar_in_set = false;
            
            for (let commit of bar.commits) {
                if (commit_ids.includes(commit['short_hash'])) {
                    bar_in_set = true;
                    break;
                }
            }

            if (bar_in_set) {
                bar.classList.add('highlight')
            } else {
                bar.classList.remove('highlight')
            }
        }

    }

    function getTargets() {
        
        return hover_targets;

    }

    function getGroupType() {

        return group_by;

    }


    // ------------------------- //
    // ---------- API ---------- //
    // ------------------------- //


    Timeline = {
        initialise,
        reset,
        loadData,
        setTargets,
        reload,
        setHighlighted,
        getTargets,
        viewScale,
        scrollView,
        getData,
        getGroupType,
    }

    return Timeline;

});