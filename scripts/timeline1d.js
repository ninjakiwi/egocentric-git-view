// Add the timeline1d module to the global object
(function(root, factory) {

    root.Timeline1D = factory();

    window.addEventListener('load', () => Timeline1D.initialise());

})(this, function() {

    'use strict';

    let Timeline1D;

    let data,
        group_by = '',
        groups = [],

        scrolling,
        scrolling_start,
        scrolling_pos,
        real_scrolling_pos = 0,

        html_container,
        html_scroller,
        html_dots,
        html_left_margin,
        html_right_margin;

    /**
     * Initialises the module, gets the target element on the page.
     */
    function initialise() {

        let timelines = document.getElementsByClassName('timeline1d');

        if (timelines.length != 1) {
            console.log('Cannot find element');
            return;
        }

        html_container = document.createElement('div');
        let html_dots_outer = document.createElement('div');
        html_dots = document.createElement('div');

        let html_scroller_container = document.createElement('div');
        html_scroller = document.createElement('div');
        html_left_margin = document.createElement('div');
        html_right_margin = document.createElement('div');

        html_dots_outer.classList.add('timeline1d-dots-outer');
        html_dots.classList.add('timeline1d-dots');
        html_container.classList.add('timeline1d-inner');
        html_scroller.classList.add('timeline1d-scroller');
        html_scroller_container.classList.add('timeline1d-scroller-container');

        html_dots_outer.appendChild(html_dots);
        html_container.appendChild(html_dots_outer);
        html_scroller_container.appendChild(html_left_margin);
        html_scroller_container.appendChild(html_scroller);
        html_scroller_container.appendChild(html_right_margin);
        html_container.appendChild(html_scroller_container);
        timelines[0].appendChild(html_container);

        window.addEventListener('egocentric-timelineReloaded', () => updateData());
        window.addEventListener('resize', () => updateScroller());

        html_scroller.addEventListener('mousedown', (event) => {
            scrolling = true;
            scrolling_start = event.clientX;
            scrolling_pos = real_scrolling_pos;
            document.body.classList.add('grabbing');
        });
        html_scroller.addEventListener('mouseup', () => {
            scrolling = false;
            document.body.classList.remove('grabbing');
        });
        window.addEventListener('mousemove', (event) => {
            if (event.buttons != 1) {
                scrolling = false;
                document.body.classList.remove('grabbing');
            }
            if (scrolling) {
                dragScroller(event.clientX);
            }
        });

    }

    /**
     * Get the group type from the main timeline and redraw
     */
    function updateData() {

        group_by = Timeline.getGroupType();
        reload();
        updateScroller();

    }

    /**
     * Load some new data and redraw.
     */
    function loadData(new_data) {

        data = new_data;
        reload();

    }

    /**
     * Update the size of the scroller according to the timeline module.
     */
    function updateScroller() {

        let size = Timeline.viewScale();
        let new_data = Timeline.getData();

        if (size > 1) {
            size = 1;
        }

        let last_group_id = groups.indexOf(getGroupID(new_data[new_data.length - 1], group_by))
        let first_group_id = groups.indexOf(getGroupID(new_data[0], group_by));
        let data_size = (last_group_id - first_group_id + 1) / groups.length;
        let bar_width = size * data_size;

        html_left_margin.style.width = ((first_group_id) / groups.length) * 100 + '%';
        html_right_margin.style.width = (1 - ((last_group_id + 1) / groups.length)) * 100 + '%';

        html_scroller.style.width = bar_width * 100 + '%';
        html_scroller.style.marginLeft = '0px';
        real_scrolling_pos = 0;

    }

    /**
     * Update the position of the scroller given the mouse position.
     */
    function dragScroller(new_mouse_pos) {

        scrolling_pos += new_mouse_pos - scrolling_start;
        real_scrolling_pos = scrolling_pos;

        let max_scroll = html_scroller.parentNode.clientWidth - html_scroller.clientWidth - html_right_margin.clientWidth - html_left_margin.clientWidth;

        if (scrolling_pos < 0) {
            real_scrolling_pos = 0;
        } else if (scrolling_pos > max_scroll) {
            real_scrolling_pos = max_scroll;
        }

        html_scroller.style.marginLeft = real_scrolling_pos + 'px';
        scrolling_start = new_mouse_pos;

        // Update the timeline graph to match
        let scroll_val = real_scrolling_pos / max_scroll;
        if (max_scroll === 0) {
            scroll_val = 0;
        }
        Timeline.scrollView(scroll_val);

    }

    /**
     * Puts data onto timeline
     */
    function reload() {

        while (html_dots.firstChild) {
            html_dots.lastChild.remove();
        }

        let biggest_dot_radius = 8;
        let smallest_dot_radius = 2;

        let previous_group = getGroupID(data[0], group_by);
        let last_group = getGroupID(data[data.length - 1], group_by);
        let full_length = (getAllGroupsBetween(previous_group, last_group, group_by).length || data.length - 2) + 1;
        let commits = [];
        let total_group_lines = 0;
        let largest_dot = 0;
        let smallest_dot = 0;
        let index = 0;

        groups = [];

        for (let commit of data) {

            let new_group = getGroupID(commit, group_by);
            if (new_group !== previous_group) {
                addDot(commits, total_group_lines, index / full_length);
                let between_groups = getAllGroupsBetween(previous_group, new_group, group_by);
                groups.push(previous_group);
                groups.push(between_groups);
                index += between_groups.length + 1;
                commits = [];
                largest_dot = Math.max(largest_dot, total_group_lines);
                smallest_dot = Math.min(smallest_dot, total_group_lines);
                total_group_lines = 0;
                previous_group = new_group;
            }

            commits.push(commit);
            let [additions, removals] = getTotalChanges(commit);
            total_group_lines += additions + removals;

        }
        addDot(commits, total_group_lines, index / full_length);
        groups.push(previous_group);
        largest_dot = Math.max(largest_dot, total_group_lines);
        smallest_dot = Math.min(smallest_dot, total_group_lines);
        
        let dots = html_dots.children;
        for (let html_dot of dots) {
            let normalised_size = (html_dot.dotsize - smallest_dot) / (largest_dot - smallest_dot);
            let size = normalised_size * (biggest_dot_radius - smallest_dot_radius) + smallest_dot_radius;
            html_dot.firstChild.style.borderWidth = size + 'px';
        }

    }

    /**
     * Adds a dot to the timeline
     */
    function addDot(commits, size, pos) {

        let new_dot = document.createElement('div');
        let new_dot_inner = document.createElement('div');
        new_dot.style.marginLeft = pos * 100 + '%';
        new_dot.dotsize = size;

        new_dot.commits = commits;

        new_dot.appendChild(new_dot_inner);
        html_dots.appendChild(new_dot);

    }

    /**
     * Given a list of commits, highlights all the matching dots on the timeline.
     */
    function setHighlighted(commits) {

        if (commits === null) {
            html_dots.classList.remove('highlight-mode');
            return
        } else {
            html_dots.classList.add('highlight-mode');
        }

        let commit_ids = commits.map((a) => a['short_hash']);
        let dots = html_dots.children;

        for (let dot of dots) {
            let dot_in_set = false;
            
            for (let commit of dot.commits) {
                if (commit_ids.includes(commit['short_hash'])) {
                    dot_in_set = true;
                    break;
                }
            }

            if (dot_in_set) {
                dot.classList.add('highlight')
            } else {
                dot.classList.remove('highlight')
            }
        }

    }


    // ------------------------- //
    // ---------- API ---------- //
    // ------------------------- //


    Timeline1D = {
        initialise,
        loadData,
        setHighlighted,
    }

    return Timeline1D;

});